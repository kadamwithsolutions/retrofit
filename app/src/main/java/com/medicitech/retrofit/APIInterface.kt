package com.medicitech.retrofit

import com.medicitech.retrofit.pojo.LoginRequestEntity
import com.medicitech.retrofit.pojo.LoginResponseEntity
import com.medicitech.retrofit.pojo.UserResponseEntity
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface APIInterface {
    @POST("/api/login")
    fun login(@Body loginRequestEntity: LoginRequestEntity) : Call<LoginResponseEntity>

    @GET("/api/users/2")
    fun  getUser() : Call<UserResponseEntity>
}