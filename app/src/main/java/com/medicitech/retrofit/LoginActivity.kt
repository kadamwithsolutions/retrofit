package com.medicitech.retrofit

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.medicitech.retrofit.pojo.LoginRequestEntity
import com.medicitech.retrofit.pojo.LoginResponseEntity
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class LoginActivity : AppCompatActivity(),Callback<LoginResponseEntity> {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        val apiInterface = APIClient.getClient().create(APIInterface::class.java)
        btn_login.setOnClickListener {
            val loginRequestEntity = LoginRequestEntity(edit_email.text.toString(), edit_password.text.toString())
            val call = apiInterface.login(loginRequestEntity)

            call.enqueue(this)
        }
    }

    override fun onResponse(call: Call<LoginResponseEntity>?, response: Response<LoginResponseEntity>?) {
        if(response!!.isSuccessful) {
            val loginRes: LoginResponseEntity = response.body()
            Toast.makeText(this@LoginActivity, loginRes.token, Toast.LENGTH_LONG).show()
            val intent: Intent = Intent(this@LoginActivity, MainActivity::class.java).apply {
                putExtra("token", loginRes.token)
            }
            startActivity(intent)
        }
    }

    override fun onFailure(call: Call<LoginResponseEntity>, t: Throwable) {
        call.cancel()
        Toast.makeText(this@LoginActivity, "Login Failed", Toast.LENGTH_LONG).show()

    }
}
