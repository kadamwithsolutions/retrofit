package com.medicitech.retrofit.pojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LoginRequestEntity (
    @Expose
    @SerializedName("email")
    val email:String="",
    @Expose
    @SerializedName("password")
    val password:String=""
    )

