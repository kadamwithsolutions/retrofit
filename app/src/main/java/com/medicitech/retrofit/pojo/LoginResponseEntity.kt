package com.medicitech.retrofit.pojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class LoginResponseEntity(
    @Expose
    @SerializedName("token")
    val token:String =""
)