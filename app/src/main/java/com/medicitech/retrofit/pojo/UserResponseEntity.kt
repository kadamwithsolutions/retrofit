package com.medicitech.retrofit.pojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class UserResponseEntity (
    @Expose
    @SerializedName("data")
    val data : User
)

data class User(
    @Expose
    @SerializedName("id")
    val id:Int,

    @Expose
    @SerializedName("first_name")
    val firstName:String ="",

    @Expose
    @SerializedName("last_name")
    val lastName:String="",

    @Expose
    @SerializedName("avatar")
    val avatar:String =""
)