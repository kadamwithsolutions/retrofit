package com.medicitech.retrofit

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.medicitech.retrofit.pojo.UserResponseEntity
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() , Callback<UserResponseEntity> {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        tv_token.text = "token :"+ intent.extras.getString("token")

        val apiInterface = APIClient.getClient().create(APIInterface::class.java)

        val call = apiInterface.getUser()

        call.enqueue(this)
    }


    override fun onResponse(call: Call<UserResponseEntity>?, response: Response<UserResponseEntity>?) {
        if(response!!.isSuccessful) {
            val user = response.body()
            tv_user.text = "First Name: ${user.data.firstName}\nLast Name: ${user.data.lastName}\nURL: ${user.data.avatar}"
        }
    }

    override fun onFailure(call: Call<UserResponseEntity>?, t: Throwable?) {
        Toast.makeText(this@MainActivity, "Login Failed", Toast.LENGTH_LONG).show()

    }

}
